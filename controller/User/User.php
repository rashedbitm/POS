<?php

namespace App\User;
use App\Message\Message;
use App\Utility\Utility;
//session_start();
use App\Model\Database as DB;
class User extends DB{
    public $id="";
    public $first_name="";
    public $email="";
    public $password="";


    public function __construct()
    {
        parent::__construct();
    }


    public function prepare($data=Array()){
        if(array_key_exists('first_name',$data)){
            $this->first_name=$data['first_name'];
        }
        if(array_key_exists('email',$data)){
            $this->email=$data['email'];
        }
        if(array_key_exists('password',$data)){
            $this->password=md5($data['password']);
        }
        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
        return $this;

    }

    public function store(){
        $query="INSERT INTO `db_pos`.`login` (`first_name`, `email`, `password`) VALUES ('".$this->first_name."','".$this->email."', '".$this->password."');";
        $result=mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
  <strong>Success!</strong> Sucessfully Registered, you can log in now.
</div>");
            Utility::redirect('../index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been stored successfully.
    </div>");
            Utility::redirect('index.php');

        }
    }

    public function index(){
        $_allUser= array();
        $query="SELECT * FROM `login`";
        $result= mysqli_query($this->conn,$query);
        //You can also use mysqli_fetch_object e.g: $row= mysqli_fetch_object($result)
        while($row= mysqli_fetch_assoc($result)){
            $_allUser[]=$row;
        }

        return $_allUser;
    }

    public function update(){
        $query="UPDATE `db_pos`.`login` SET `first_name` = '".$this->first_name."', `email` ='".$this->email."' `password` ='".$this->password."' WHERE `login`.`id` =".$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-info\">
  <strong>Updated!</strong> Data has been Updated successfully.
</div>");
            header('Location:index.php');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been updated  successfully.
    </div>");
            Utility::redirect('index.php');

        }

    }

    public function delete(){
        $query="DELETE FROM `db_pos`.`login` WHERE `login`.`id` = ".$this->id;
        $result= mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class=\"alert alert-success\">
  <strong>Deleted!</strong> Data has been deleted successfully.
</div>");
            header('Location:../../view/user/users.typephp');

        } else {
            Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been deleted successfully.
    </div>");
            Utility::redirect('../../view/user/users.typephp');

        }
    }

    public function view(){
        $query="SELECT * FROM `login` WHERE `id`=".$this->id;
        $result= mysqli_query($this->conn,$query);
        $row= mysqli_fetch_assoc($result);
        return $row;
    }
    
}