<div id="sidebar-menu">
                        <ul>
                            <li class="">
                            <a href="<?php $_SERVER['DOCUMENT_ROOT'] ?>/pos/View/index.php"></i> <span> Dashboard </span></a>
                            </li>
                            <li class="has_sub">
                                <a href="#" class="waves-effect"><i class="ti-server"></i><span>Products </span></a>
                                <ul class="list-unstyled">
                                    <li><a href="<?php $_SERVER['DOCUMENT_ROOT'] ?>/pos/View/Product/product-create.php">New Product</a></li>
                                    <li><a href="<?php $_SERVER['DOCUMENT_ROOT'] ?>/pos/View/Product/product-list.php">Product List</a></li>
                                    <li><a href="<?php $_SERVER['DOCUMENT_ROOT'] ?>/pos/View/ProductCat/product-cat-create.php">Add Category</a></li>
                                    <li><a href="<?php $_SERVER['DOCUMENT_ROOT'] ?>/pos/View/ProductCat/product-category-list.php">Category List</a></li>
                                    <li><a href="<?php $_SERVER['DOCUMENT_ROOT'] ?>/pos/View/ProductSize/product-size-create.php">Add Size</a></li>
                                    <li><a href="<?php $_SERVER['DOCUMENT_ROOT'] ?>/pos/View/ProductSize/product-size-list.php">Size List</a></li>
                                </ul>
                            </li>
                            <li class="has_sub">
                                <a href="#" class="waves-effect"><i class="ti-bookmark-alt"></i><span> Purchase </span></a>
                                <ul class="list-unstyled">
                                    <li><a href="<?php $_SERVER['DOCUMENT_ROOT'] ?>/pos/View/Purchase/purchase-create.php">Add Purchase</a></li>
                                    <li><a href="<?php $_SERVER['DOCUMENT_ROOT'] ?>/pos/View/Purchase/purchase-list.php">Purchase List</a></li>
                                </ul>
                            </li>
                            <li class="has_sub">
                                <a href="#" class="waves-effect"><i class="ti-bookmark-alt"></i><span> Sales </span></a>
                                <ul class="list-unstyled">
                                    <li><a href="<?php $_SERVER['DOCUMENT_ROOT'] ?>/pos/View/Sales/sales-create.php">New Sale</a></li>
                                    <li><a href="<?php $_SERVER['DOCUMENT_ROOT'] ?>/pos/View/Sales/sales-list.php">sales list</a></li>
                                </ul>
                            </li>
                            <li class="has_sub">
                                <a href="#" class="waves-effect"><i class="ti-linux"></i> <span> Customer </span> </a>
                                <ul class="list-unstyled">
                                    <li><a href="<?php $_SERVER['DOCUMENT_ROOT'] ?>/pos/View/Customer/customer-create.php">Add Customer</a></li>
                                    <li><a href="<?php $_SERVER['DOCUMENT_ROOT'] ?>/pos/View/Customer/customer-list.php">Customer List</a></li>
                                    <li><a href="<?php $_SERVER['DOCUMENT_ROOT'] ?>/pos/View/CustomerCat/customer-type.php">Add Customer Type</a></li>
                                    <li><a href="<?php $_SERVER['DOCUMENT_ROOT'] ?>/pos/View/CustomerCat/customers-type-list.php">Customer Type List</a></li>
                                </ul>
                            </li>

                           <!-- <li class="has_sub">
                                <a href="#" class="waves-effect"><i class="ti-light-bulb"></i><span> Items </span> </a>
                                <ul class="list-unstyled">
                                    <li><a href="junk/components-grid.html">New Items</a></li>
                                    <li><a href="junk/components-widgets.html">List Of Items</a></li>

                                </ul>
                            </li>-->
                            <li class="has_sub">
                                <a href="#" class="waves-effect"><i class="ti-spray"></i> <span> Supplier </span> </a>
                                <ul class="list-unstyled">
                                	<li><a href="<?php $_SERVER['DOCUMENT_ROOT'] ?>/pos/View/Supplier/create-suppliers.php">Supplier</a></li>
                                    <li><a href="<?php $_SERVER['DOCUMENT_ROOT'] ?>/pos/View/Supplier/supplier-list.php">Supplie List</a></li>
                                   
                                </ul>
                            </li>
							
							<!--<li class="has_sub">
                                <a href="#" class="waves-effect"><i class="ti-spray"></i> <span> Company </span> </a>
                                <ul class="list-unstyled">
                                	<li><a href="suppliers.php">Companies </a></li>
                                    <li><a href="create-suppliers.php">New Company</a></li>
                                   
                                </ul>
                            </li>-->

                            <li class="has_sub">
                                <a href="#" class="waves-effect"><i class="ti-bookmark-alt"></i><span>All Reports </span></a>
                                <ul class="list-unstyled">
                                    <li><a href="<?php $_SERVER['DOCUMENT_ROOT'] ?>/pos/View/Supplier/suppliers_report.php">Supplier</a></li>
                                    <li><a href="<?php $_SERVER['DOCUMENT_ROOT'] ?>/pos/View/Customer/customer_report.php">Customer </a></li>
                                    <li><a href="<?php $_SERVER['DOCUMENT_ROOT'] ?>/pos/View/Inventory/inventory_report.php">Inventory</a></li>
                                    <li><a href="<?php $_SERVER['DOCUMENT_ROOT'] ?>/pos/View/Sales/sales_report.php">Sales </a></li>
                                  <!--  <li><a href="<?php /*$_SERVER['DOCUMENT_ROOT'] */?>/pos/View/Customer/sales_return_report.php">Sales Return </a></li>-->
                                    <li><a href="<?php $_SERVER['DOCUMENT_ROOT'] ?>/pos/View/Purchase/purchase_report.php">Purchase</a></li>
                                  <!--  <li><a href="<?php /*$_SERVER['DOCUMENT_ROOT'] */?>/pos/View/Customer/purchase_return.php">Purchase Return </a></li>-->

                                    
                                </ul>
                            </li>
                         <!--   <li class="has_sub">
                                <a href="#" class="waves-effect"><i class="ti-cut"></i><span> Expenses </span></a>
                                <ul class="list-unstyled">
                                	<li><a href="junk/chart-flot.html">Add</a></li>
                                    <li><a href="junk/chart-morris.html">List Of Expenses</a></li>
                                    
                                </ul>
                            </li>

                            <li class="has_sub">
                                <a href="#" class="waves-effect"><i class="ti-location-pin"></i><span> Employees </span></a>
                                <ul class="list-unstyled">
                                    <li><a href="map-google.html"> New Employee</a></li>
                                    <li><a href="email-templates/map-vector.html"> List of Employees</a></li>
                                </ul>
                            </li>
-->
                            <li class="has_sub">
                                <a href="#" class="waves-effect"><i class="ti-settings"></i><span> configuration  </span></a>
                                <ul class="list-unstyled">
                                    <li><a href="<?php $_SERVER['DOCUMENT_ROOT'] ?>/pos/View/User/users.php"> User</a></li>
                                    <!--<li><a href="<?php /*$_SERVER['DOCUMENT_ROOT'] */?>/pos/View/User/user-edit.php">User Edit</a></li>-->
                                    <li><a href="<?php $_SERVER['DOCUMENT_ROOT'] ?>/pos/View/Usertype/user-type-create.php">Add User Type</a></li>
                                    <li><a href="<?php $_SERVER['DOCUMENT_ROOT'] ?>/pos/View/Usertype/users-type.php">User Type List</a></li>
                                    <li><a href="<?php $_SERVER['DOCUMENT_ROOT'] ?>/pos/View/Company/company-details.php">Company Details</a></li>
                                </ul>
                            </li>

                        </ul>
                        <div class="clearfix"></div>
                    </div>