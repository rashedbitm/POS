<?php
include_once('../../vendor/autoload.php');
use App\Sales\Sales;
use App\Utility\Utility;


$product = new Sales();
//Utility::dd($_POST);
$customer = $_POST['customer'];
$singleCustomer = implode(',',$customer);
$_POST['customer']= $singleCustomer;

$products = $_POST['product'];
$singleProduct = implode(',',$products);
$_POST['product']= $singleProduct;

$category = $_POST['CategoryID'];
$singleCategory = implode(',',$category);
$_POST['CategoryID']= $singleCategory;

$productSize = $_POST['size'];
$singleSize = implode(',',$productSize);
$_POST['size']= $singleSize;
//var_dump($_POST);
//die();
$product->prepareData($_POST)->create();
