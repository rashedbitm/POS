<?php
include_once('../../vendor/autoload.php');
use App\ProductCat\ProductCat;
use App\ProductSize\ProductSize;
use App\CustomerCreate\CustomerCreate;

$product = new ProductCat();
$getAllProductCat= $product->prepareData($_GET)->index();

$product_size = new \App\ProductSize\ProductSize();
$getAllProductSize = $product_size->prepareData($_GET)->index();

$product = new App\Product\Product();
$getAllProduct= $product->prepareData($_GET)->index();

$customer = new CustomerCreate();
$getAllCustomer= $customer->prepare($_GET)->index();

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <meta name="author" content="Coderthemes">

    <link rel="shortcut icon" href="../assets/images/favicon_1.ico">

    <title>Ubold - Responsive Admin Dashboard Template</title>
    <!-- Plugins css-->
    <link href="../assets/plugins/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
    <link href="../assets/plugins/switchery/dist/switchery.min.css" rel="stylesheet" />
    <link href="../assets/plugins/multiselect/css/multi-select.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/select2/select2.css" rel="stylesheet" type="text/css" />
    <link href="../assets/plugins/bootstrap-select/dist/css/bootstrap-select.min.css" rel="stylesheet" />
    <link href="../assets/plugins/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />

    <link href="../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/core.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/components.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/pages.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />


    <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <script src="../../assets/js/modernizr.min.js"></script>

</head>

<body class="fixed-left">

<!-- Begin page -->
<div id="wrapper">

    <!-- Top Bar Start -->
    <?php include_once('../header.php');?>
    <!-- Top Bar End -->


    <!-- ========== Left Sidebar Start ========== -->

    <div class="left side-menu">
        <div class="sidebar-inner slimscrollleft">
            <!--- Divider -->
            <?php include_once('../menu.php'); ?>
            <div class="clearfix"></div>
        </div>
    </div>
    <!-- Left Sidebar End -->

    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container">

                <!-- Page-Title -->



                <div class="row">
                    <div class="col-sm-12">
                        <div class="card-box">
                            <h4 class="m-t-0 header-title"><b>Sales</b></h4>

                            <div class="row">
                                <div class="col-md-12">
                                    <form class="form-horizontal" role="form" method="post" action="store.php">
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Customer</label>
                                            <div class="col-md-2">
                                                <select class="form-control" name="customer[]">
                                                    
                                                    <?php foreach($getAllCustomer as $customer) {?>
                                                        <option value="<?php echo $customer['customer_id'] ?>"><?php echo $customer['customer_name'] ?></option>
                                                    <?php  }  ?>
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <input type="text" class="form-control" name="customerName" value="">
                                            </div>

                                        </div>
                                         <div class="form-group">
                                            <label class="col-md-2 control-label">Product Cat</label>

                                                <div class="col-md-2">
                                                    <select class="form-control"  name="CategoryID[]">
                                                        <option>----Select----</option>
                                                        <?php foreach($getAllProductCat as $category) {  ?>
                                                            <option value="<?php echo  $category['category_id'] ?>"><?php echo $category['category_name'] ?></option>
                                                        <?php }?>
                                                    </select>
                                                </div>

                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Product</label>
                                            <div class="col-md-2">
                                                <select class="form-control" name="product[]">
                                                    <option>----Select----</option>
                                                    <?php foreach($getAllProduct as $product) {?>
                                                        <option value="<?php echo $product['product_id'] ?>"><?php echo $product['product_name'] ?></option>
                                                    <?php  }  ?>
                                                </select>

                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Product Size</label>
                                            <div class="col-md-2">
                                                <select class="form-control" name="size[]">
                                                    <option>----Select----</option>
                                                    <?php foreach($getAllProductSize as $size) {?>
                                                        <option value="<?php echo $size['product_size_id'] ?>"><?php echo $size['product_size_name'] ?></option>
                                                    <?php  }  ?>
                                                </select>

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Sell Price</label>
                                            <div class="col-md-2">
                                                <input type="text" class="form-control" name="salePrice" value="">
                                            </div>
                                        </div>
                                       <div class="form-group">
                                            <label class="col-md-2 control-label">Quantity</label>
                                            <div class="col-md-2">
                                                <input type="text" class="form-control" name="quantity" value="">
                                            </div>

                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Discount</label>
                                            <div class="col-md-2">
                                                <input type="text" class="form-control" name="discount" value="">(Discount By Taka)
                                            </div>

                                        </div>
                                       <!-- <div class="form-group">
                                            <label class="col-md-2 control-label">Sell Date</label>
                                            <div class="col-md-6">
                                                <input type="date" class="form-control" name="name" value="">
                                            </div>
                                        </div>-->
                                      <!--  <div class="form-group">
                                            <label class="col-md-2 control-label">User</label>
                                            <div class="col-md-6">
                                                <input type="text" class="form-control" name="name" value="">
                                            </div>
                                        </div>-->


                                      
                                        <div class="form-group ">
                                            <div class="col-sm-offset-2 col-sm-9">
                                                <button type="submit" class="btn btn-info waves-effect waves-light">Submit</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>




                            </div>
                        </div>
                    </div>
                </div>







            </div> <!-- container -->

        </div> <!-- content -->

        <?php include_once('../footer.php'); ?>

    </div>
    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->


    <!-- Right Sidebar -->

    <!-- /Right-bar -->


</div>
<!-- END wrapper -->

<script>
    var resizefunc = [];
</script>

<!-- jQuery  -->
<!-- jQuery  -->
<script src="../assets/js/jquery.min.js"></script>
<script src="../assets/js/bootstrap.min.js"></script>
<script src="../assets/js/detect.js"></script>
<script src="../assets/js/fastclick.js"></script>
<script src="../assets/js/jquery.slimscroll.js"></script>
<script src="../assets/js/jquery.blockUI.js"></script>
<script src="../assets/js/waves.js"></script>
<script src="../assets/js/wow.min.js"></script>
<script src="../assets/js/jquery.nicescroll.js"></script>
<script src="../assets/js/jquery.scrollTo.min.js"></script>

<script type="text/javascript" src="../assets/plugins/multiselect/js/jquery.multi-select.js"></script>
<script type="text/javascript" src="../assets/plugins/jquery-quicksearch/jquery.quicksearch.js"></script>
<script src="../assets/plugins/select2/select2.min.js" type="text/javascript"></script>
<script src="../assets/plugins/bootstrap-select/dist/js/bootstrap-select.min.js" type="text/javascript"></script>


<script src="../assets/js/jquery.core.js"></script>
<script src="../assets/js/jquery.app.js"></script>

<!--form validation init-->
<script src="../assets/plugins/tinymce/tinymce.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        if($("#elm1").length > 0){
            tinymce.init({
                selector: "textarea#elm1",
                theme: "modern",
                height:300,
                plugins: [
                    "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                    "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                    "save table contextmenu directionality emoticons template paste textcolor"
                ],
                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons",
                style_formats: [
                    {title: 'Bold text', inline: 'b'},
                    {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
                    {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
                    {title: 'Example 1', inline: 'span', classes: 'example1'},
                    {title: 'Example 2', inline: 'span', classes: 'example2'},
                    {title: 'Table styles'},
                    {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
                ]
            });
        }
    });
</script>

<script>
    jQuery(document).ready(function() {

        //advance multiselect start
        $('#my_multi_select3').multiSelect({
            selectableHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='search...'>",
            selectionHeader: "<input type='text' class='form-control search-input' autocomplete='off' placeholder='search...'>",
            afterInit: function (ms) {
                var that = this,
                    $selectableSearch = that.$selectableUl.prev(),
                    $selectionSearch = that.$selectionUl.prev(),
                    selectableSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selectable:not(.ms-selected)',
                    selectionSearchString = '#' + that.$container.attr('id') + ' .ms-elem-selection.ms-selected';

                that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                    .on('keydown', function (e) {
                        if (e.which === 40) {
                            that.$selectableUl.focus();
                            return false;
                        }
                    });

                that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                    .on('keydown', function (e) {
                        if (e.which == 40) {
                            that.$selectionUl.focus();
                            return false;
                        }
                    });
            },
            afterSelect: function () {
                this.qs1.cache();
                this.qs2.cache();
            },
            afterDeselect: function () {
                this.qs1.cache();
                this.qs2.cache();
            }
        });

        // Select2
        $(".select2").select2();

        $(".select2-limiting").select2({
            maximumSelectionLength: 2
        });

        $('.selectpicker').selectpicker();
        $(":file").filestyle({input: false});
    });

    //Bootstrap-TouchSpin
    $(".vertical-spin").TouchSpin({
        verticalbuttons: true,
        verticalupclass: 'ion-plus-round',
        verticaldownclass: 'ion-minus-round'
    });
    var vspinTrue = $(".vertical-spin").TouchSpin({
        verticalbuttons: true
    });
    if (vspinTrue) {
        $('.vertical-spin').prev('.bootstrap-touchspin-prefix').remove();
    }

    $("input[name='demo1']").TouchSpin({
        min: 0,
        max: 100,
        step: 0.1,
        decimals: 2,
        boostat: 5,
        maxboostedstep: 10,
        postfix: '%'
    });
    $("input[name='demo2']").TouchSpin({
        min: -1000000000,
        max: 1000000000,
        stepinterval: 50,
        maxboostedstep: 10000000,
        prefix: '$'
    });
    $("input[name='demo3']").TouchSpin();
    $("input[name='demo3_21']").TouchSpin({
        initval: 40
    });
    $("input[name='demo3_22']").TouchSpin({
        initval: 40
    });

    $("input[name='demo5']").TouchSpin({
        prefix: "pre",
        postfix: "post"
    });
    $("input[name='demo0']").TouchSpin({});


    //Bootstrap-MaxLength
    $('input#defaultconfig').maxlength()

    $('input#thresholdconfig').maxlength({
        threshold: 20
    });

    $('input#moreoptions').maxlength({
        alwaysShow: true,
        warningClass: "label label-success",
        limitReachedClass: "label label-danger"
    });

    $('input#alloptions').maxlength({
        alwaysShow: true,
        warningClass: "label label-success",
        limitReachedClass: "label label-danger",
        separator: ' out of ',
        preText: 'You typed ',
        postText: ' chars available.',
        validate: true
    });

    $('textarea#textarea').maxlength({
        alwaysShow: true
    });

    $('input#placement') .maxlength({
        alwaysShow: true,
        placement: 'top-left'
    });
</script>


</body>
</html>