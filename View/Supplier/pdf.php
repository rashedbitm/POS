<?php
require_once ('../../vendor/mpdf/mpdf/mpdf.php');
include_once ('../../vendor/autoload.php');
use App\Supplier\Supplier;

$suppliers = new Supplier();

$getAllSuppliers = $suppliers->prepare($_GET)->index();
$mpdf = new mPDF();
$tr ="";
$sl= 0;
foreach ($getAllSuppliers as $allSuppliers):
    $sl++ ;
    $tr .="<tr>";
    $tr .="<td>".$sl ."</td>";
    $tr .="<td>".$allSuppliers['supplier_name']."</td>";
    $tr .="<td>".$allSuppliers['supplier_address']."</td>";
    $tr .="<td>".$allSuppliers['supplier_phone']."</td>";
    $tr .="<td>".$allSuppliers['supplier_email']."</td>";
    $tr .="<td>".$allSuppliers['supplier_bank_account']."</td>";
    $tr .="<td>".$allSuppliers['supplier_status']."</td>";
    $tr .= "</tr>";
endforeach;
// Write some HTML code:
$html=<<<EOD
<!DOCTYPE html>
<html>

<body class="fixed-left">
<div id="wrapper">
    <div class="content-page" id="donotprintdiv">
        <!-- Start content -->
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-default">

                            <style>
                                .logo-center{text-align: center;}

                            </style>
                            <div class="panel-body">
                                <div class="clearfix">
                                    <div class="col-md-12">
                                        <h4 class="logo-center"><img src="../assets/images/logo_dark.png" alt="velonic" ></h4>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="logo-center">
                                            <address>
                                                795 Folsom Ave, Suite 600,
                                                San Francisco, CA 94107,
                                                <abbr title="Phone">P:</abbr> (123) 456-7890
                                            </address>
                                            <h4 class="page-title">Supplier Report</h4>
                                        </div>
                                        <div class="pull-right m-t-30">
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="m-h-50"></div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="table m-t-30">
                                                <thead>
                                                <tr><th>#</th>
                                                    <th> Name</th>
                                                    <th>Address</th>
                                                    <th>Email</th>
                                                    <th>Phone</th>
                                                    <th>Bank Info</th>
                                                    <th>Status</th>

                                                </tr></thead>
                                                <tbody>
                                                    $tr;
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <hr>
                                
                            </div>
                        </div>

                    </div>

                </div>

            </div> 
        </div>

    </div>
</div>
</body>
</html>
EOD;
$mpdf->WriteHTML($html);

// Output a PDF file directly to the browser
$mpdf->Output();