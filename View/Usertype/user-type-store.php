<?php

include_once('../../vendor/autoload.php');
use App\Types\Usertypes;
use App\Message\Message;
use App\Utility\Utility;
if((isset($_POST['user_type_name']))&& (!empty($_POST['user_type_name']))) {
    $userType = new Usertypes();
    $userType->prepare($_POST);
    $userType->store();

}
else {
    Message::message("<div class=\"alert alert-danger\">
  <strong>Error!</strong> Data has not been stored successfully.
    </div>");
    Utility::redirect('../../view/usertype/user-type.php');

}