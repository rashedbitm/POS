<?php
session_start();
include_once('../vendor/autoload.php');
use App\User\User;
use App\User\Auth;
use App\Message\Message;
use App\Utility\Utility;

$auth= new Auth();
$status= $auth->prepare($_POST)->is_exist();
if($status){
    header('Location: ../index.php');
}else{
    $obj= new User();
    $obj->prepare($_POST)->store();
}
